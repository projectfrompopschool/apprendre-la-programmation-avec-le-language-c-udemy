// stdio permet d'afficher du text à l'écran
#include<stdio.h>
#include<stdlib.h>

/*
fonction main, est la fonction principal de notre programme

elle retourne un 0  si tout se passe bien
*/
int main(){
    printf("hello world");
    printf("\t\t\t\t\t apprendre a coder en \"C\"\n");
    return 0;
}