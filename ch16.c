#include<stdio.h>
#include<stdlib.h>

int main()
{
    int mon_int = 12;
    int taille_int = sizeof(int);
    printf("mon int = %d et prend en memoire %d d'octets\n", mon_int, taille_int);

    //pour un seul caractère cela doit etre des simple cote sinon cela ne marche pas
    char mon_char = 'A';
    printf("mon int %c et prend en memoire %d d'octets\n", mon_char, sizeof(char));

    float mon_float = 3.1416;
    printf("mon float = %f et prend en memoire %d d'octets\n", mon_float, sizeof(float));

    double mon_double = 12.6;
    printf("mon double = %f et prend en memoire %d d'octets\n", mon_double, sizeof(double));

    return 0;
}